terraform {
    required_providers {
        azurerm = "hashicorp/azurerm"
        version = "2.66.0"
    }
}
required_version   = "0.14"
